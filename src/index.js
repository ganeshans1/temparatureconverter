import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TemparatureCalculator from './App';
import registerServiceWorker from './registerServiceWorker';
import  ReactDOMServer from 'react-dom/server.browser'

ReactDOM.render(<TemparatureCalculator/>, document.getElementById('root'));
registerServiceWorker();

console.log(ReactDOMServer.renderToString(<TemparatureCalculator/>));
//nsole.log(ReactDOMServer.renderToStaticMarkup(<TemparatureCalculator/>));
